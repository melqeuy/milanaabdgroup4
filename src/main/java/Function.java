public class Function {

    public static void main(String[] args) {
        int arr[] = {1, 3, 4, 13, 5};
        int numb = 4;
        System.out.println(indexFromMassive(arr, numb));

    }

    public static int indexFromMassive(int[] array, int number) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                return i;
            }

        }
        return -1;
    }
}
